﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nakatomi.Common.NPDM
{
    public struct OffsetSize
    {
        public uint Offset;
        public uint Size;
    }
}