﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nakatomi.Common.NPDM
{
    [Flags]
    public enum AddressSpaceFlags : byte
    {
        /// <summary>
        /// Standard 32-bit address space.
        /// </summary>
        AddressSpace32Bit = 0,

        /// <summary>
        /// Unknown/unused flag. Potentially 64-bit with no reserved memory?
        /// </summary>
        Unused = 1,

        /// <summary>
        /// 32-bit process that ignores all memory space rules. Unused.
        /// </summary>
        AddressSpace32BitNoReservedMemory = 2,

        /// <summary>
        /// Standard 64-bit address space. (You want this.)
        /// </summary>
        AddressSpace64Bit = 3
    }
}