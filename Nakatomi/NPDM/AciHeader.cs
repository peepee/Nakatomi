﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace Nakatomi.Common.NPDM
{
    /// <summary>
    /// Struct AciHeader. Describes the ACI0 format that is used as the first layer of signature validation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct AciHeader
    {
        /// <summary>
        /// The header magic/signature. Always should be ACI0.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] Magic;

        /// <summary>
        /// An unknown block that is always set to 0's. Considered to be padding.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public byte[] Padding_Unknown_1;

        /// <summary>
        /// The application title identifier.
        /// </summary>
        public ulong TitleId;

        /// <summary>
        /// An unknown block that is always set to 0's. Considered to be padding.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public byte[] Padding_Unknown_2;

        /// <summary>
        /// The filesystem access offset and size info.
        /// </summary>
        public OffsetSize FilesystemAccess;

        /// <summary>
        /// The service access control offset and size info.
        /// </summary>
        public OffsetSize ServiceAccessControl;

        /// <summary>
        /// The kernel access control offset and size info.
        /// </summary>
        public OffsetSize KernelAccessControl;

        /// <summary>
        /// An unknown block that is always set to 0's. Considered to be padding.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public byte[] Padding_Unknown_3;
    }
}