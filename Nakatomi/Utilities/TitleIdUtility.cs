﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nakatomi.Common.Utilities
{
    public static class TitleId
    {
        public static string HexSwap(string titleId)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < titleId.Length; i += 4)
            {
                sb.Append($"{titleId[i + 2]}{titleId[i + 3]}{titleId[i]} {titleId[i + 1]}");
            }

            return sb.ToString();
        }

        public static ulong ToLong(string titleId)
        {
            return Convert.ToUInt64(titleId, 16);
        }

        public static string ToHex(ulong titleId)
        {
            return $"{titleId:X}";
        }
    }
}